FROM ruby:latest
RUN apt-get update \
  && apt-get install -y git
RUN git clone https://bitbucket.org/ridnarong/thrift-monitor.git
RUN cd thrift-monitor \
  && bundle install
WORKDIR thrift-monitor
CMD ["ruby","thrift_service.rb"]
